INCLUDE Irvine32.inc

.data
numb1 sdword ?
numb2 sdword ?
val sdword -1
msg1 byte "Enter the first number: ",0
msg2 byte "Enter the second number: ",0
msgres byte "Result is: ",0
 

.code

main PROC

	mov edx,offset msg1
	call writestring     ;cout message
	call readint         ;first number is in eax
	mov numb1,eax          ;first number is in numb1
	mov ebx,numb1          ;first number is in ebx
	
	

    mov edx,offset msg2
	call writestring     ;cout message
	call readint         ;second number is in eax
	mov numb2,eax        ;second number is in numb2
	mov ecx,numb2        ;second number is in ecx


	;Check if any of the numbers equal zero

	.if ebx==0            ;if first number = 0
	mov edx,offset msgres ;mov result message
	call writestring      ;cout message
	mov eax,ecx           ;move second number to eax
	call writeint         ;cout value in eax
	call crlf             ;endl	
	jmp fin

	.elseif ecx==0        ;if second number =0
	mov edx,offset msgres ;mov result message
	call writestring      ;cout message
	mov eax,ebx           ;move first number to eax
	call writeint         ;cout value in eax
	call crlf             ;endl
	jmp fin
	.endif


	.IF numb1<0          ;check if first number is -ve
	mov eax,numb1
	imul val           ;make it +ve
	mov ebx,eax
	.ENDIF

	.IF numb2<0          ;check if second number is -ve
	mov eax,numb2
	imul val           ;make it +ve
	mov ecx,eax
	.ENDIF



	call GCD           ;Call GCD function

	

	

	fin:

	exit
	main ENDP

	GCD proc           ; proc of GCD

	;Check which number is smaller

	.if ebx<ecx        ;check if first number is smaller than the second one
	mov esi,ebx        ;smaller numb is in esi
	mov edi,ecx        ;bigger numb is in edi
	.elseif ecx<=ebx    ;check if second number is smaller than or equal the first numb
	mov esi,ecx        ;smaller numb is in esi
	mov edi,ebx        ;bigger numb is in edi
	.endif
	

	.while esi!=0      ;iterating

	mov eax,edi        ;move biggest number to eax
	mov edx,0          ;make edx equal 0
	cdq                ;extend the registers
	div esi            ; divide by esi
	mov edi,edx        ;move remainder to edi
	xchg esi,edi       ;xchange values
	.endw              ;end loop

	mov eax,edi           ;call result
	mov edx,offset msgres ;move message
	call writestring
	call writedec
	call crlf


	ret
	GCD ENDP

END main